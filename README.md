* [x] Feature 1*: Configure your CI tool to build and bundle electron-secure-defaults. Creating a .zip, .tar, or other archive is sufficient to “bundle”.
* [x] Feature 2*: Create a notification after the build. It should include * A URL that points to the build in CI. * The status of the build * Other information you deem relevant. You may choose to have the CI system email you the notification or you can also choose to mock the notification in a job.
* [x] Feature 3*: electron-secure-defaults has some old dependencies, and some vulnerabilities. Create a job where vulnerabilities of high or critical are found.
* [x] Feature 4*: The output of dist/ is difficult to run. Package the application in a way that a customer on macOS, Windows, or Linux would expect to run. You may want to use the pre configured electron-builder but feel free to substitute if you are familiar with similar tools (ex. Electron Forge)
        Note: We only expect one platform, of your choice.
        Note: We are not concerned with code signing for this exercise.
        Be prepared to discuss what you did, the structure of the package as well as demonstrate starting the app via screen share during the interview.
* [ ] Feature 5*: Consider what other changes you could make to accomplish the following goals. Implement or describe how you would make these improvements.
  - produce higher quality software
  - deliver faster
  - ease the on-boarding of new developers
