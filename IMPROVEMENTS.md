# Improvements:

## Overall
- Have imported based on project
- Could be imported based on built in CI variable: CI_PROJECT_REPOSITORY_LANGUAGES (LIMITATION OF 5 LANGUAGES)
 - Above could be imprved with this: https://docs.gitlab.com/ee/api/projects.html#languages
- Use a local registry with local runners
 - This ensures we dont need to have external dependencies
- Enable CI/CD Catalog
 - Allows version control of components
- Look more into how to use the Electron Scripts on `package.json`
 - Maybe would have made some runners easier.
- Read more into: https://docs.gitlab.com/runner/configuration/speed_up_job_execution.html#:~:text=GitLab%20Container%20Registry.-,Use%20a%20distributed%20cache,the%20distributed%20runners%20cache%20feature

 ## Build
- Test Cases
- Smaller Components
- Better commit messaging
- More inputs for variability
  - More DRY 

## Notify
- Get working on an SMTP relay API
- Look into Solution space more for email and SMTP via pipelines.

 ## Vulnerabilities
- add rule for all
- break down each strategy into components

## Electron Builder
- Become more familiar with it
- Set up runners for Mac/Windows/Linux